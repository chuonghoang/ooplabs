// Предметная область
// 4 марта 2023 г.
// Автор: chuonghoang

#include <iostream>
#include <vector>

using namespace std;

class Buyer // аргументы события
{
public:
    string name;
    int age;
    int amountOfMoney;
    string productName; // товар, который он хочет купить
};

class Product
{
public:
    string name;
    float price;
    bool adultsOnly;

    virtual void willBeBoughtBy(Buyer buyer)
    {
        if (name == buyer.productName) {
            if (buyer.amountOfMoney >= price)
            {
                if (adultsOnly == true)
                {
                    if (buyer.age >= 18)
                    {
                        cout << buyer.name << " kupil uspeshno " << name << endl;
                    }
                    else
                    {
                        cout << buyer.name << " ne mozhet kupit' vzroslyy produkt!" << endl;
                    }
                }
                else
                {
                    cout << buyer.name << " kupil uspeshno " << name << endl;
                }
            }
            else
            {
                cout << buyer.name << " ne khvatayet deneg!" << endl;
            }
        }
    }
};

class Stock
{
public:
    vector<Product*> Stock;
};

class Store : public Buyer, public Stock
{
public:
    vector<Product*> getStock()
    {
        return Stock::Stock;
    }
    void importGoods(Product* product)
    {
        Stock::Stock.push_back(product);
    }
};

int main()
{
    Store store;

    Product* milk = new Product;
    Product* bread = new Product;
    Product* cigarette = new Product;

    milk->name = "milk"; milk->price = 121, 99; milk->adultsOnly = false;
    bread->name = "bread"; bread->price = 49, 99; bread->adultsOnly = false;
    cigarette->name = "cigarette"; cigarette->price = 149, 99; cigarette->adultsOnly = true;

    store.importGoods(milk);
    store.importGoods(bread);
    store.importGoods(cigarette);

    Buyer chuong{ "Chuong", 20, 100, "milk" };
    Buyer ivan{ "Ivan", 17, 300, "cigarette" };
    Buyer anton{ "Anton", 24, 50, "bread" };

    for (auto& product : store.getStock())
    {
        product->willBeBoughtBy(chuong);
    }
    for (auto& product : store.getStock())
    {
        product->willBeBoughtBy(ivan);
    }
    for (auto& product : store.getStock())
    {
        product->willBeBoughtBy(anton);
    }
}
