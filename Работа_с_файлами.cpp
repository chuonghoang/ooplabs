﻿// struct NOTE
// {
// std::string name; // ФИО
// std::string tele; // телефон
// std::chrono::year_month_day birthday; // день рождения
// };

// Написать функцию, которая читает данные о записи из файла notes.txt в структуру NOTE.
// Структура notes.txt: первые 4 байта(целое) — число записей в файле; далее записи в формате структуры NOTE.
// Написать функцию, которая выводит на экран информацию о человеке, чьи дни рождения приходятся на месяц, значение которого введено с клавиатуры.
// Если такого нет, то программа должна предложить ввести другой месяц и повторить поиск людей.

// 21 января 2023 года, суббота
// Автор: chuonghoang

#include <fstream>
#include <iostream>
#include <chrono>
#include <string>

using namespace std;
using namespace std::chrono;

struct NOTE
{
    string name;
    string tele;
    chrono::year_month_day birthday;
};

int numberOfPeople;
NOTE listOfPeople[10000];

void createFileNotesDotTXT()
{
    srand((unsigned)time(0));
    int numberOfPeople = rand() % 10 + 10;
    string names[100] = { "Ivan", "Chuong", "Petp", "Alek", "Lina", "Sergey", "Oleg", "Anton", "Ethan", "Mem", "Dark", "Bruh", "Anan", "Anna", "King", "Bob" };
    ofstream fileOut("notes.txt");

    fileOut << numberOfPeople << endl;
    for (int i = 0; i < numberOfPeople; ++i)
    {
        string name = names[rand() % 16];
        string tele = to_string(80000000000 + ((rand()*rand()) % 10000000000));
        year_month_day birthday{ (month)(unsigned int)(rand() % 12 + 1) / (rand() % 28 + 1) / (rand() % 120 + 1900) };
         
        fileOut << name << "\t" << tele << "\t" << birthday << endl;
    }
}

void readFileNotesDotTXT()
{
    ifstream fileIn("notes.txt");
    fileIn >> numberOfPeople;
    for (int i = 0; i < numberOfPeople; ++i)
    {
        int name, tele, fYear, fMonth, fDay;
        char slash;
        fileIn >> listOfPeople[i].name >> listOfPeople[i].tele;
        fileIn >> fYear >> slash >> fMonth >> slash >> fDay;
        year_month_day fBirthday{ (year)(fYear) / (month)(fMonth) / (day)(fDay) };
        listOfPeople[i].birthday = fBirthday;
    }
}

void userInterface()
{
    int fMonth;
    bool isFounded = false;

    while (isFounded == false)
    {
        cout << "Please enter month: " << endl;
        cin >> fMonth;
        if (cin.fail())
        {
            cin.clear();
            cout << "Error!" << endl;
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
        }
        else
        {

            for (int i = 0; i < numberOfPeople; ++i)
            {
                if (fMonth == static_cast<unsigned>(listOfPeople[i].birthday.month()))
                {
                    cout << listOfPeople[i].name << "\t" << listOfPeople[i].tele << "\t" << listOfPeople[i].birthday << endl;
                    isFounded = true;
                }
            }
        }
        if (isFounded == false)
        {
            cout << "Do not found!" << endl;
        }
    }
}

int main()
{
    createFileNotesDotTXT();
    readFileNotesDotTXT();
    userInterface();
}
