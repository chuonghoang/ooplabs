﻿// Сформировать массив строк. Удалить из него строку с заданным номером.
// 14 января 2023 г.
// Автор: chuonghoang

#include <iostream>
#include <string>
using namespace std;

int main()
{
    string massiv[10000];
    int dlinaMassiva = NULL;
    int nomer;

    cout << "n = ";

    while (true)
    {
        cin >> dlinaMassiva;
        if (cin.fail())
        {
            cin.clear();
            cout << "Oshibka!" << endl;
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
        }
        else
        {
            break;
        }
    }
    
    for (int i = 1; i <= dlinaMassiva; i++)
    {
        cin >> massiv[i];
    }

    cout << "Vvedite nomer stroki, kotoruyu nuzhno udalit': ";

    while (true)
    {
        cin >> nomer;
        if (cin.fail() || nomer <= 0 || nomer > dlinaMassiva)
        {
            cin.clear();
            cout << "Oshibka! nomer dolzhno byt' chislom, kotoroye men'she dliny massiva i bol'she nulya." << endl;
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
        }
        else
        {
            break;
        }
    }

    for (; nomer <= dlinaMassiva; ++nomer)
    {
        massiv[nomer] = massiv[nomer+1];
    }

    cout << "Massiv = ";
    for (int i = 1; i <= dlinaMassiva; i++)
    {
        cout << massiv[i] << " ";
    }

    return 0;
}