﻿// В матрице размером NxM поменять местами строку, содержащую элемент с наибольшим значением, со строкой, содержащей элемент с наименьшим значением.
// 20 января 2023 г.
// Автор: chuonghoang

#include <iostream>
#include <string>
using namespace std;

const int INF = 10000;

int n, m;
int massiv[INF][INF];
int minOfLine[INF], maxOfLine[INF];
int maxLine = -INF, minLine = INF;
int maxLineOrder = 0, minLineOrder = 0;

void naydiStroki()
{
    n = 4;
    m = 4;
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= m; j++)
        {
            massiv[i][j] = rand() % 100;
            cout << massiv[i][j] << '\t';
            if (minOfLine[i] > massiv[i][j])
            {
                minOfLine[i] = massiv[i][j];
            }
            if (maxOfLine[i] < massiv[i][j])
            {
                maxOfLine[i] = massiv[i][j];
            }
        }
        cout << endl;
        if (minLine > minOfLine[i])
        {
            minLine = minOfLine[i];
            minLineOrder = i;
        }
        if (maxLine < maxOfLine[i])
        {
            maxLine = maxOfLine[i];
            maxLineOrder = i;
        }
    }
}

void pomenyatMestamiStroki()
{
    for (int i = 1; i <= n; i++)
    {
        int vremennyy = massiv[minLineOrder][i];
        massiv[minLineOrder][i] = massiv[maxLineOrder][i];
        massiv[maxLineOrder][i] = vremennyy;
    }
}

void printMassiv()
{
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= m; j++)
        {
            cout << massiv[i][j] << '\t';
        }
        cout << endl;
    }
}

int main()
{
    srand((unsigned int)time(NULL));
    cout << "go" << endl;
    naydiStroki();
    cout << "posle" << endl;
    pomenyatMestamiStroki();
    printMassiv();
    return 0;
}